 let buttonChangeTheme = document.querySelector("#change-theme");
let changedTheme = localStorage.getItem("theme") === "true";
function applyTheme() {
  if (changedTheme) {
    document.querySelector(`span`).style.color = `greenyellow`;
    let menu = document.querySelectorAll(`.header-menu-container`);
    menu.forEach((li) => (li.style.backgroundColor = `green`));
    document.querySelectorAll(".link-of-menu-sidebar").forEach((link) => (link.style.color = `green`));
    buttonChangeTheme.style.cssText = `background-color: green; border: green`;
    document.querySelectorAll(`.block-image`).forEach((block) => (block.style.backgroundColor = `greenyellow`));
    document.querySelectorAll(`.link-of-bottom`).forEach((link) => {
      link.classList.remove(`link-of-bottommenu-first`);
      link.classList.add(`link-of-bottommenu-second`);
    });
  } else {
    document.querySelector(`span`).style.color = `rgba(80, 204, 226, 0.977)`;
    let menu = document.querySelectorAll(`.header-menu-container`);
    menu.forEach((li) => (li.style.backgroundColor = `#35444F`));
    document.querySelectorAll(".link-of-menu-sidebar").forEach((link) => (link.style.color = `#35444F`));
    buttonChangeTheme.style.cssText = `background-color: #3181A6;; border:#3181A6;`;
    document.querySelectorAll(`.block-image`).forEach((block) => (block.style.backgroundColor = ``));
    document.querySelectorAll(`.link-of-bottom`).forEach((link) => {
      link.classList.add(`link-of-bottommenu-first`);
      link.classList.remove(`link-of-bottommenu-second`);
    });
  }
}
buttonChangeTheme.addEventListener("click", () => {
  changedTheme = !changedTheme;
  localStorage.setItem("theme", changedTheme.toString());
  applyTheme(); 
});

applyTheme(); 